from django import forms
from django.forms import ModelForm
from .models import Friend

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'birth_date']
        widgets = {'birth_date':DateInput()}
