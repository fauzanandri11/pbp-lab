from django.urls import include, path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_note, name='add-note'),
    path('note-list', note_list, name='note-list'),
    
    
]
