from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()

    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/lab-4')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes = Note.objects.all().values()

    response = {'notes' : notes}
    return render(request, 'lab4_note_list.html', response)