from django.apps import AppConfig

class PbpappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pbpapp'
