from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Person(models.Model):
    display_name = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=17)
 
class Post(models.Model):
    author = models.ForeignKey(Person, on_delete = models.CASCADE)
    content = models.CharField(max_length=125)
    published_date = models.DateTimeField(default=timezone.now)
