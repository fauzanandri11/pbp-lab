from django.shortcuts import render
from django.http import HttpResponse
from .models import Person, Post

# Create your views here.


mhs_name = 'Zan'

def index(request):
    persons = Person.objects.all().values()

    response = {'name' : mhs_name, 'persons' : persons}
    return render(request, 'index.html', response)