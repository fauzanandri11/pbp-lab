Perbedaan antara JSON dan XML diantaranya:
1. JSON tidak menggunakan end tag
	Struktur JSON mirip seperti tuple pada python, sehingga tidak
	menggunakan end tag untuk menandakan akhir elemen
2. JSON lebih pendek daripada XML
	Hal ini dikarenakan penulisan JSON tidak memerlukan penggunaan
	end tag
3. JSON bisa menggunakan array
	Sehingga penulisannya menjadi lebih ringkas dan tidak ber-
	ulang
4. XML harus di parse menggunakan parser XML, sedangkan JSON bisa
	di parse menggunakan fungsi JavaScript standard

Sumber:  
https://www.javatpoint.com/html-vs-xml#:~:text=HTML.%20XML.%201%29%20HTML%20is%20used%20to%20display,XML%20provides%20a%20framework%20to%20define%20markup%20languages.

Perbedaan antara HTML dan XML diantaranya:
1. HTML digunakan untuk menampilkan data dan berfokus pada bagaimana data
	tersebut ditampilkan. Sedangkan XML adalah sebuah tool yang independen, digunakan
	untuk memindahkan dan menyimpan data, dan berfokus pada apa data tersebut
2. HTML adalah sebuah markup language, sedangkan XML menyediakan framework untuk
	mendefinisikan markup language
3. HTML tidak case-sensitive, sedangkan XML case-sensitive
4. HTML adalah sebuah presentation language, sedangkan XML bukanlah sebuah presentation
	language dan bukan juga programming language
5. HTML punya tag yang pre-defined sedangkan pada XML kita bisa mendefinisikan tag sesuai
	kebutuhan
6. closing tag wajib digunakan pada XML, sedangkan tidak wajib pada HTML
7. HTML bersifat static karena digunakan untuk menampilkan data, sedangkan XML bersifat
	dinamis karena digunakna untuk memindahkan data
8. HTML  tidak memperhatikan whitespace, sedangkan HTML memperhatikan whitespace

Sumber:
https://www.javatpoint.com/html-vs-xml#:~:text=HTML.%20XML.%201%29%20HTML%20is%20used%20to%20display,XML%20provides%20a%20framework%20to%20define%20markup%20languages.