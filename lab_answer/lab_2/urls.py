from django.urls import include, path
from .views import *

urlpatterns = [
    path('/', index, name='index'),
    path('/xml', xml, name='xml'),
    path('/json', json, name='json'),
]
